local json=require('json');
local log = require('log');
local fun = require('fun');


local CommentsChanelFeed = {

}

function CommentsChanelFeed.truncate()
    box.space.comments_chanel_feed:truncate();
end

local function init()

    local space_name_chanel_feed = 'comments_chanel_feed';
    local user = 'app';


    local space = box.space[space_name_chanel_feed];
    local space_exist = (space ~= nill);

    if not space_exist then


        --   Структура:
        --      - id - id комментария (см. ICommentsDBService).
        --      - entity_type - id типа канала (сообщение, статья и т.п.)
        --     - entity_id - (строка) id щбъекта

        space = box.schema.space.create(space_name_chanel_feed)
        space:create_index('primary', { type = 'tree',unique = true, parts = {1, 'NUM',2, 'STR', 3, 'NUM'} });
        --space:create_index('section', { type = 'tree',unique = false, parts = {2, 'NUM', 3, 'STR'} });


        box.schema.user.grant(user, 'read,write,execute', 'space', space_name_chanel_feed);

    end







    log.info('Инициализируем спейсы для Comments');

end

return {
    init = init,
    CommentsChanelFeed = CommentsChanelFeed
};