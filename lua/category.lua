local json=require('json');
local log = require('log');
local fun = require('fun');



local category = {}

function category.init()

    local space_name = 'category';
    local user = 'guest';
    local pass = '123';

    local space = box.space[space_name];
    local space_exists = (space ~= nil);
    if not space_exists then
        space = box.schema.space.create(space_name)
        space:create_index('primary', { type = 'hash',unique = true, parts = {1, 'NUM'} });
        space:create_index('section', { type = 'tree',unique = false, parts = {2, 'NUM', 3, 'NUM', 4, 'NUM'} });

        --space:create_index('owner', { type = 'tree',unique = false, parts = {2, 'NUM', 4, 'NUM'} });
    end
    local user_exist = box.schema.user.exists(user)


    if not user_exist then
        box.schema.user.create(user)  --     box.schema.user.create(user, {password=pass})

    end

    if not space_exists or not user_exist then
        --box.schema.user.grant(user, 'read,write', 'space', space_name)
        box.schema.user.grant(user, 'read,write,execute', 'universe', space_name)
    end



end

function category.init2()

    local space_name = 'category';
    local user = 'guest';
    local pass = '123';

    local space = box.space[space_name];
    local space_exists = (space ~= nil);
    if not space_exists then
        space = box.schema.space.create(space_name)
        space:create_index('primary');
        space:create_index('owner', { type = 'tree',unique = false, parts = {2, 'NUM', 3, 'NUM'} });--ownerType, ownerId
        --space:create_index("primary", {type = 'hash', parts = {1, 'NUM'}}) -- id
        --space:create_index("secondary", {type = 'tree', parts = {2, 'NUM'}}) -- session create/update timestamp
    end
    local user_exist = box.schema.user.exists(user)


    if not user_exist then
        box.schema.user.create(user)  --     box.schema.user.create(user, {password=pass})

    end

    if not space_exists or not user_exist then
        --box.schema.user.grant(user, 'read,write', 'space', space_name)
        box.schema.user.grant(user, 'read,write,execute', 'universe', space_name)
    end

    print('category.init');

end

function category.hello()
    return {d=1};
end

function category.addObject(ownerType, ownerId, sections, objectType, objectId, date)
    local space_name = 'category';
    local space = box.space[space_name];
    --  log.info('category.addObject ' .. id);
    for key, value in ipairs(sections)
    do
        local id = space:len()+1;
        space:insert{id, ownerType, ownerId, value, objectType, objectId, date};

    end

end

function category.addObjects(arObjects)
end

--возвращает количество сообщений по каждой категории. Фильтр по владельцу.
function category.getCounts(ownerType, ownerId)
    local space_name = 'category';
    local space = box.space[space_name];
    local sel = space.index.owner:select({ownerType, ownerId}, { iterator = 'EQ'});--, limit = 1000

    local result = {};
    for key, value in ipairs(sel)
    do

        local sec = value[4];
        if result[sec] == nil then
            result[sec] = 0;
        end

        result[sec] = result[sec] + 1;

    end
    return result;
end


--category.getCounts(1,2 )

return category;
--category.init();