<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20.12.2015
 * Time: 21:28
 */

namespace SergeyMZR\Category;


class Memcache implements ICache{
    public function getAmounts($ownerType, $ownerId){
        return array("_fromCache"=>true);
    }


    public function setAmounts($ownerType, $ownerId, $arAmounts){

    }


    public function onUpdateAmountFor($ownerType, $ownerId, $idSection, $amount){

    }


    public function getObjectsInBasket($ownerType, $ownerId, $section, $basket = null){
        return array("_fromCache"=>true);
    }

    public function setObjectsInBasket($ownerType, $ownerId, $section, $basket, $arObjects){

    }

    public function onSetObjectsInBasket($ownerType, $ownerId, $idSection, $arObjects, $basketNumber, $isLast){

    }
}