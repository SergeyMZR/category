<?php
namespace SergeyMZR\Category;

use Phalcon\Events\EventsAwareInterface;
use Phalcon\Events\ManagerInterface;

/*
 * Используем Phalcon\Db\Adapter\Pdo\Mysql для написание запросов
 */

class MySql implements EventsAwareInterface{


    public static $cEVENT_NAME = 'CategoryMySql';

    public static $cAMOUNT_IN_BASKET= 30;

    /**
     * @var ManagerInterface
     */
    protected $_eventsManager;

    public function setEventsManager(ManagerInterface $eventsManager)
    {
        $this->_eventsManager = $eventsManager;
    }

    /**
     * Returns the internal event manager
     *
     * @return ManagerInterface
     */
    public function getEventsManager()
    {
        return $this->_eventsManager;
    }

    /**
     * Конструктор.
     *
     * Устанавливаем соединение на чтение и запись. DI
     * @param $dbRead
     * @param $dbWright
     */
    public function __construct($dbRead, $dbWright){

    }

    /**
     * Создает таблицы, если она не существует.
     * Используется только в служебных скриптах и тестах
     */
    public function createTable(){


        /*
                CREATE TABLE categorizer (
                  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                  owner varchar(25) NOT NULL COMMENT 'Владелец. Например: ''1:150''',
                  category int(11) UNSIGNED NOT NULL COMMENT 'Id категории',
                  amount int(8) UNSIGNED NOT NULL COMMENT 'Количество объектов в категории',
                  baskets int(7) UNSIGNED NOT NULL COMMENT 'Количество корзин',
                  lastBasket varchar(320) NOT NULL COMMENT 'Последняя корзина',
                  PRIMARY KEY (id),
                  UNIQUE INDEX UK_categorizer (owner, category)
                )
                ENGINE = INNODB
                AUTO_INCREMENT = 1
                CHARACTER SET utf8
                COLLATE utf8_general_ci;
         */

        /*

                CREATE TABLE baskets (
                  id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                  owner varchar(25) NOT NULL COMMENT 'Владелец',
                  basket int(7) UNSIGNED NOT NULL COMMENT 'Номер корзины',
                  list varchar(320) NOT NULL,
                  PRIMARY KEY (id),
                  UNIQUE INDEX UK_baskets (owner, basket)
                )
                ENGINE = INNODB
                AUTO_INCREMENT = 1
                CHARACTER SET utf8
                COLLATE utf8_general_ci;
         */

    }

    /**
     * Удаляет таблицы, если она существует.
     * Используется только в служебных скриптах и тестах
     */
    public function dropTable(){
        //categorizer, baskets
    }

    public function addObject($ownerType, $ownerId, array $arSections, $objectType, $objectId){

        $owner = $ownerType.':'.$ownerId;
        $object = array($objectType, $objectId);

        //1. Пытаемся найти категории для владельца

        foreach($this->_getCategories($owner, $arSections) as $idSection=>$fields){

            if($fields === null){
                //Данной категории у владельца еще не было
                $this->_addNewCategory($ownerType, $ownerId, $idSection, $object);
            }else{

                //todo доработать

                if(count($fields['lastBasket'])<self::$cAMOUNT_IN_BASKET){

                    //количество объектов в последней корзине < self::$cAMOUNT_IN_BASKET

                    $this->_addObjectToCategory($ownerType, $ownerId, $idSection, $object);

                }else{

                    /*
                     * В последней корзине всегда должно быть $cAMOUNT_IN_BASKET
                     */

                    //добавляем корзину, т.е. в корзине по 10 объектов
                    $this->_addBasket($ownerType, $ownerId,$idSection, $fields['baskets']+1, $fields['lastBasket']);
                    //обновляем категорию
                    $this->_updateCategory($ownerType, $ownerId, $idSection, $object);


                }
            }

        }
    }




    /**
     * Возвращает количество объектов по каждой категории. Фильтр по владельцу.
     * @param $ownerType
     * @param $ownerId
     * @return array
     */
    public function getAmounts($ownerType, $ownerId){


        return array(
            "owner"=>"1:156",
            "sections"=>array(
                "id_section"=>"кол.во сообщений",
                "45"=>124
                //...
            )
        );

    }

    /**
     * Возвращает список объектов в корзине.
     * @param $ownerType
     * @param $ownerId
     * @param $section
     * @param $basket
     * @return array
     */
    public function getObjectsInBasket($ownerType, $ownerId, $section, $basket = null){


        //
        $owner = $ownerType.':'.$ownerId;

        $lResult = $this->_getCategories($owner, $section);

        if($lResult === false){
            //нет сообщений в данной категории
            return array(
                "baskets"=>0,
                "owner_type"=>$ownerType,
                "owner_id"=>$ownerId,
                "amount"=>0,
                "objects"=> array()
            );

        }else{
            $lResult = $lResult[0];
            if($basket === $lResult['baskets'] || $basket === null){
                //если запросили последнюю корзину или номер корзины не передали (т.е. последнюю)
                return array(
                    "isLast"=>true,
                    "baskets"=>$lResult['baskets'],//кол-во корзин
                    "owner_type"=>$ownerType,
                    "owner_id"=>$ownerId,
                    "amount"=>$lResult['amount'], //- общее количество сообщений по данной категории. Из
                    "objects"=> $lResult['lastBasket'] //из таблицы baskets
                );
            }else{

                return array(
                    "isLast"=>false,
                    "baskets"=>$lResult['baskets'],//кол-во корзин
                    "owner_type"=>$ownerType,
                    "owner_id"=>$ownerId,
                    "amount"=>$lResult['amount'], //- общее количество сообщений по данной категории. Из
                    "objects"=> $this->_getBasket($owner, $basket) //из таблицы baskets
                );

            }
        }


    }



    /**
     * Возвращает записи из таблицы categorizer
     * @param $owner
     * @param array $arSections
     * @return array
     */
    public function _getCategories($owner, array $arSections){
        /*
         * Выборка одним запросом.
         * Затем дополняем теми категориями, которых нет.
         */
        return array(
            "id_section"=>array(/*все поля из таблицы categorizer*/),
            "id2"=>null /*категории не существует*/
        );

    }

    /**
     * Добавляем новую запись в таблицу categorizer
     * @param $ownerType
     * @param $ownerId
     * @param $idSection
     * @param $object
     */
    public function _addNewCategory($ownerType, $ownerId, $idSection, $object){

        //amount = 1 - Количество объектов в категории
        //baskets = 1 - Количество корзин
        //lastBasket = array($object)

        /*
         * Если при выполнении запроса произойдет ошибка, что данная запись уже существует
         */
        $isError = false;
        if($isError){
            $this->_addObjectToCategory($ownerType, $ownerId, $idSection, $object);
        }


        if(isset($this->_eventsManager)){
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onUpdateAmountFor', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'idSection'=>$idSection, 'amount'=>1));
        }

    }

    /**
     * Добавляет объект в сущ. категорию
     * @param $ownerType
     * @param $ownerId
     * @param $idSection
     * @param $object
     */
    public function _addObjectToCategory($ownerType, $ownerId, $idSection, $object){
        //amount = amount + 1 - Увеличиваем количество объектов в категории
        //lastBasket = array_push($object)

        if(isset($this->_eventsManager)){
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onUpdateAmountFor', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'idSection'=>$idSection, 'amount'=>$amount));
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onSetObjectsInBasket', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'basketNumber'=>$basketNumber, 'idSection'=>$idSection, "arObjects"=>$arObjects,'isLast'=>true));
        }
    }

    /**
     * Обновим категорию
     * @param $owner
     * @param $idSection
     * @param $object
     */
    public function _updateCategory($ownerType, $ownerId, $idSection, $object){
        //amount = amount + 1 - Увеличиваем количество объектов в категории
        //lastBasket = array($object)
        //baskets = baskets + 1 - Количество корзин

        if(isset($this->_eventsManager)){
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onUpdateAmountFor', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'idSection'=>$idSection, 'amount'=>$amount));
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onSetObjectsInBasket', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'basketNumber'=>$basketNumber, 'idSection'=>$idSection, "arObjects"=>$arObjects,'isLast'=>true));
        }
    }


    /**
     * Добавить запись в таблицу baskets
     * @param $owner
     * @param $basketNumber
     * @param $arObject
     */
    public function _addBasket($ownerType, $ownerId, $idSection, $basketNumber, $arObjects){

        if(isset($this->_eventsManager)){
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onSetObjectsInBasket', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'basketNumber'=>$basketNumber, 'idSection'=>$idSection, "arObjects"=>$arObjects,'isLast'=>false));
        }

    }


    /**
     * Возвращает запись из baskets
     * @param $owner
     * @param $basketNumber
     * @return array
     */
    public function _getBasket($owner, $basketNumber){
        return array();
    }


    /**
     * Удалить объект
     * @param $ownerType
     * @param $ownerId
     * @param array $arSections
     */
    public function deleteObject($ownerType, $ownerId, array $arSections, $objectType, $objectId){

        //в таблице categorizer уменьшаем кол-во сообщений
        //если categorizer.lastBasket - если содержиться данный объект, то удалаем его.
        //Если в categorizer.lastBasket - останется 0, то удалить корзину из таблицу baskets(если корзин больше 1) или данную запись. Подумать!


        if(isset($this->_eventsManager)){
            $this->_eventsManager->fire(self::$cEVENT_NAME . ':onUpdateAmountFor', $this, array('ownerType'=>$ownerType, 'ownerId'=>$ownerId, 'idSection'=>$idSection, 'amount'=>$amount));
        }
    }

    public function test(){

        return true;
    }
}