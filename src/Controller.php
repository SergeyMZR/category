<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 20.12.2015
 * Time: 20:35
 */

namespace SergeyMZR\Category;


class Controller {


    public $dbRead;
    public $dbWright;
    public $eventsManager;
    
    public function __construct($dbRead, $dbWright, $eventsManager){

    }

    public function addObject($ownerType, $ownerId, array $arSections, $objectType, $objectId){


        $mysql = new MySql($this->$dbRead, $this->$dbWright);
        $mysql->setEventsManager($this->$eventsManager);
        $mysql->addObject($ownerType, $ownerId, $arSections, $objectType, $objectId);
        //todo Memcache подписать на события в MySql
    }



    public function getAmounts($ownerType, $ownerId){


        //1. Смотрим в кэше
        $cache = new Memcache();
        $lResult = $cache->getAmounts($ownerType, $ownerId);
        if($lResult!==false){
            //есть данные в кэше
            return $lResult;
        }else{
            //данных в кеше нет
            $mysql = new MySql();
            $lResult = $mysql->getAmounts($ownerType, $ownerId);
            $cache->setAmounts($ownerType, $ownerId, $lResult);

            return $lResult;
        }
    }

    public function getObjectsInBasket($ownerType, $ownerId, $section, $basket = null){

        //1. Смотрим в кэше
        $cache = new Memcache();
        $lResult = $cache->getObjectsInBasket($ownerType, $ownerId, $section, $basket);
        if($lResult!==false){
            //есть данные в кэше
            return $lResult;
        }else{
            //данных в кеше нет
            $mysql = new MySql();
            $lResult = $mysql->getObjectsInBasket($ownerType, $ownerId, $section, $basket);
            $cache->setObjectsInBasket($ownerType, $ownerId, $section, $basket, $lResult);

            return $lResult;
        }

    }

    public function deleteObject($ownerType, $ownerId, array $arSections, $objectType, $objectId){

    }
}