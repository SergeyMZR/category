<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 03.02.2016
 * Time: 17:13
 */

namespace SergeyMZR\Category;


class Tarantool {



    public static function isSpaceExist(){

    }



    public static function truncate(\Tarantool $connection, $sSpaceName){
        $connection->evaluate("box.space.$sSpaceName:truncate()");
    }

    public static function drop(\Tarantool $connection, $sSpaceName){
        $connection->evaluate("box.space.$sSpaceName:drop()");
    }

    public static function createIndex(\Tarantool $connection, $sSpaceName, $sIndexName, $sType = "TREE", $bUnique = true, $arParts = array(1, 'NUM'), $bIfNotExists = false){



        $sParts = '';
        for($i=0; $i<count($arParts); $i++){

            if($sParts !== ''){
                $sParts = $sParts . ',';
            }
            $value = $arParts[$i];
            if(is_string($value)){
                $sParts = $sParts . "'$value'";
            }else{
                $sParts = $sParts . "$value";
            }
        }
        $sEval = "__space = box.space.".$sSpaceName."; "
            . "__space:create_index('$sIndexName', { type = '$sType',unique = ".($bUnique===true?"true":"false").", parts = {".$sParts."}, if_not_exists=".($bIfNotExists===true?"true":"false")." });";

        try{

            $connection->evaluate("log.info(\"Пытаемся создать index: ".$sEval."\")");
            $connection->evaluate($sEval);
            $connection->evaluate("log.info('index создан: ".$sSpaceName." : ".$sIndexName."')");

            return true;

        }catch (\Exception $_e) {

            return false;

        }
    }
//local __space = box.space.test;
//__space:create_index('primary', { type = 'tree',unique = true, parts = {1,'NUM'}, if_not_exists=false });

    public static function createSpace(\Tarantool $connection, $sSpaceName, $arOptions = array()){


        $sOptions = '';
        if(count($arOptions)>0){

            $tmpArOptions = array();
            if(isset($arOptions['temporary'])){
                //temporary 	space is temporary 	boolean 	false
                array_push($tmpArOptions, 'temporary = ' . ($arOptions['temporary']===true?'true':'false'));
            }
            //id 	unique identifier 	number 	last space's id, +1
            if(isset($arOptions['id'])){
                array_push($tmpArOptions, 'id = ' . $arOptions['id']);
            }

            //field_count 	fixed field count 	number 	0 i.e. not fixed
            if(isset($arOptions['field_count'])){
                array_push($tmpArOptions, 'field_count = ' . $arOptions['field_count']);
            }

            //if_not_exists 	no error if duplicate name 	boolean 	false
            if(isset($arOptions['if_not_exists'])){
                array_push($tmpArOptions, 'if_not_exists = ' . ($arOptions['if_not_exists']===true?'true':'false'));
            }

            //engine 	storage package 	string 	'memtx'
            if(isset($arOptions['engine'])){
                array_push($tmpArOptions, 'engine = \'' . $arOptions['engine']."'");
            }

            //user 	user name 	string 	current user's name
            if(isset($arOptions['user'])){
                array_push($tmpArOptions, 'user = \'' . $arOptions['user']."'");
            }

            //todo format 	field names+types 	table 	(blank)

            $sOptions = implode(',', $tmpArOptions);

        }

        $sEval = "box.schema.space.create('".$sSpaceName."', {".$sOptions."})";

        try{

            $connection->evaluate("log.info(\"Пытаемся создать space: ".$sEval."\")");
            $connection->evaluate($sEval);
            $connection->evaluate("log.info('Space создан: ".$sSpaceName."')");

            $lResult = $connection->evaluate(" return box.space.".$sSpaceName.".id");
            return $lResult[0];

        }catch (\Exception $_e) {

            return false;

        }



    }

}