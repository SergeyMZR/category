<?php

/*
 * Интерфейс кэширования
 */

namespace SergeyMZR\Category;


interface ICache {


    /*++++++++++++++++++++++++++++++++++++++++++
     * КЭШИРОВАНИЕ MySql->getAmounts
     ++++++++++++++++++++++++++++++++++++++++++*/

    /**
     * Возвращает количество объектов по каждой категории. Фильтр по владельцу.
     * @param $ownerType
     * @param $ownerId
     * @return array
     */
    public function getAmounts($ownerType, $ownerId);


    /**
     * Установим количество объектов по каждой категории.
     * @param $ownerType
     * @param $ownerId
     * @return mixed
     */
    public function setAmounts($ownerType, $ownerId, $arAmounts);

    /**
     * Событие: Обновить кол-во объектов для переданной категории.
     * Если конечно данные есть в кэше.
     * @param $ownerType
     * @param $ownerId
     * @param $idSection
     * @param $amount
     * @return mixed
     */
    public function onUpdateAmountFor($ownerType, $ownerId, $idSection, $amount);


    /*++++++++++++++++++++++++++++++++++++++++++
    * КЭШИРОВАНИЕ MySql->getObjectsInBasket
    ++++++++++++++++++++++++++++++++++++++++++*/

    /**
     * Возвращает список объектов в корзине.
     * @param $ownerType
     * @param $ownerId
     * @param $section
     * @param null $basket
     * @return array
     */
    public function getObjectsInBasket($ownerType, $ownerId, $section, $basket = null);


    /**
     * Событие: установить список объктов для владельца и категории
     * @param $ownerType
     * @param $ownerId
     * @param $idSection
     * @param $arObjects
     * @param $isLast
     * @return mixed
     */
    public function onSetObjectsInBasket($ownerType, $ownerId, $idSection, $arObjects, $basketNumber, $isLast);


    /**
     * Установить объекты в корзине
     * @param $ownerType
     * @param $ownerId
     * @param $section
     * @param $basket
     * @param $arObjects
     * @return mixed
     */
    public function setObjectsInBasket($ownerType, $ownerId, $section, $basket, $arObjects);

}