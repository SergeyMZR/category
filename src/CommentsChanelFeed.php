<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 02.02.2016
 * Time: 21:53
 */

namespace SergeyMZR\Category;


class CommentsChanelFeed {


    /**
     * @var $_connection \Tarantool
     */
    private $_connection;

    private static $cSPACE_NAME = 'comments_chanel_feed';

    private static $cSPACE_NAME_CHANEL = 'comments_chanel';


    public function initShema(){


        /*
         * comments_chanel_feed
         *    --   Структура:
        - entity_type - id типа канала (сообщение, статья и т.п.)
        - entity_id - (строка) id объекта
         - id - id комментария (см. ICommentsDBService).
         */
        $id = Tarantool::createSpace($this->_connection, self::$cSPACE_NAME, array('user'=>'app', 'if_not_exists'=>true));
        Tarantool::createIndex($this->_connection, self::$cSPACE_NAME , 'primary', 'tree', true, array(1, 'NUM',2, 'STR', 3, 'NUM'), true);

        /*
        * comments_chanel
        *    Структура:
       - entity_type - id типа канала (сообщение, статья и т.п.)
       - entity_id - (строка) id объекта
        - num - количество комментариев
        */
        $id = Tarantool::createSpace($this->_connection, self::$cSPACE_NAME_CHANEL, array('user'=>'app', 'if_not_exists'=>true));
        Tarantool::createIndex($this->_connection, self::$cSPACE_NAME_CHANEL , 'primary', 'tree', true, array(1, 'NUM',2, 'STR'), true);
        $id = Tarantool::createSpace($this->_connection, 'ddddddddd', array('user'=>'app', 'engine'=>'sophia', 'if_not_exists'=>true));
    }

    public function setConnection(\Tarantool $connection){
        $this->_connection = $connection;
    }


    public function truncate(){
        Tarantool::truncate($this->_connection,  self::$cSPACE_NAME_CHANEL);
        Tarantool::truncate($this->_connection,  self::$cSPACE_NAME);
    }


    public function add($nEntityType, $sEntityId, $nCommentId, $sText){


        $nEntityType = intval($nEntityType);
        $sEntityId = strval($sEntityId);
        $nCommentId = intval($nCommentId);

        //

        $arChanel = $this->_connection->select(self::$cSPACE_NAME_CHANEL, array($nEntityType, $sEntityId), "primary");
        if(count($arChanel) === 0){
            $this->_connection->insert(self::$cSPACE_NAME_CHANEL,array($nEntityType, $sEntityId, 1));
        }else{
            $this->_connection->update(self::$cSPACE_NAME_CHANEL,array($nEntityType, $sEntityId), array(array(
                "field" => 2,
                "op" => "+",
                "arg" => 1
            )), "primary");
        }

        $this->_connection->insert(self::$cSPACE_NAME,array($nEntityType, $sEntityId, $nCommentId,  $sText));

    }

    public function getAll($nEntityType, $sEntityId){
        $sEntityId = strval($sEntityId);
        return $this->_connection->select(self::$cSPACE_NAME, array($nEntityType, $sEntityId), "primary", null, null, TARANTOOL_ITER_EQ);
    }

}